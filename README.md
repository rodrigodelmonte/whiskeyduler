# README #

Whiskeyduler schedules docker job in AWS EC2 spot instances. This solution uses the services AWS API Gateway, Lambda and DynamoDB.

### Requirements ###

Unfortunately AWS Cloudformation does not support AWS API Gateway yet, so we use the tool terraform from hashcorp and the aws-cli to create the environment.

* [terraform](https://www.terraform.io/)
* [AWS CLI](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)

### Install Requirements ###

* terraform

```
sudo wget https://releases.hashicorp.com/terraform/0.6.14/terraform_0.6.14_linux_amd64.zip
sudo unzip terraform_0.6.14_linux_amd64.zip -d /usr/local/bin/
```
* AWS CLI

```
sudo apt-get install python-pip -y
OR
sudo yum install python-pip -u
sudo pip install awscli -y
aws configure
#PROVIDE your credentials
```

### Create the AWS envitonment ###

You have to setup some envitonments variables.
```
export AWS_ACCESS_KEY_ID=AXXXXXXXXXXXXXXXX
export AWS_SECRET_ACCESS_KEY=W48MXXXXXXXXXXXXXXXXXXXXXXXXXr
export AWS_REGION=us-west-2
```
Enter in **terraform** folder and execute the command below:
```
terraform apply
```
If you get some error messages, please execute the command again. AWS creates some asynchronous services and this behaviour can cause some errors because the components should not be ready.

### Testing ###

The docker job whiskey-ping-job receives some env vars like SITE and PING_COUNT to test the communication then upload the test result to the S3_BUCKET.

#### SCHEDULING ####
```
curl https://THE_API_ID.execute-api.us-west-2.amazonaws.com/prod/schedule -X POST -H \
'Content-Type: application/json' -d @example_schedule_request.json

```
Example json example_schedule_request.json
```
{
	"image": "rodrigodelmonte/whiskey-ping-job",
	"datetime": "2016-04-02",
	"env": [{
		"AWS_ACCESS_KEY_ID": "AXXXXXXXXXXXXXXQ",
		"AWS_SECRET_ACCESS_KEY": "WXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXr",
		"S3_BUCKET": "whiskeyduler-output-job",
		"SITE": "www.google.com",
		"PING_COUNT": "10"
	}]
}
```

#### LIST JOBS ####
```
curl https://THE_API_ID.execute-api.us-west-2.amazonaws.com/prod/list -X GET -H \
'Content-Type: application/json'
```

#### CHECKING STATUS JOB ####
```
curl https://THE_API_ID.execute-api.us-west-2.amazonaws.com/prod/status -X POST -H \
'Content-Type: application/json' -d '{ "job": "THE JOB ID"}'

```
