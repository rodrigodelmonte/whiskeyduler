#!/bin/sh

ping -c $PING_COUNT $SITE > /tmp/$SITE-$DATETIME.txt
aws s3 cp /tmp/$SITE-$DATETIME.txt s3://$S3_BUCKET/$SITE-$DATETIME.txt
