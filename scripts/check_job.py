#!/usr/bin/env python
from docker import Client
from time import sleep
import requests
import json
import boto3

API = "https://PASTE_THE_API_ID_HERE.execute-api.us-west-2.amazonaws.com/prod/callback"
docker = Client(base_url='unix://var/run/docker.sock')
termination_endpoint = "http://169.254.169.254/latest/meta-data/spot/termination-time"
ec2 =  boto3.client('ec2', region_name="us-west-2")
instance_id = requests.get("http://169.254.169.254/latest/meta-data/instance-id")
instance_id = instance_id.content


def terminate():

    spot = ec2.describe_spot_instance_requests(
            Filters=[{'Name': 'instance-id', 'Values': [instance_id]}])
    spot_request_id = spot['SpotInstanceRequests'][0]['SpotInstanceRequestId']
    ec2.terminate_instances(InstanceIds=[instance_id])
    ec2.cancel_spot_instance_requests(SpotInstanceRequestIds=[spot_request_id])


while True:

    data = {}
    status = docker.containers( all=True )[0]['Status']
    job = docker.containers( all=True )[0]['Names'][0][1:]
    data['job'] = job
    container_running = docker.containers()

    # Check container job.
    if not container_running:
        if status.startswith('Exited (0)'):
            data['job_status'] = "FINISHED"
            data = json.dumps(data)
            requests.post( API, data=data)
            terminate()
        else:
            data['job_status'] = "FAILED"
            data = json.dumps(data)
            requests.post( API, data=data)
            terminate()

    # Check spot instance market.
    termination = requests.get(termination_endpoint)
    if termination == 200:
        data['job_status'] = "RESCHEDULE"
        data = json.dumps(data)
        requests.post( API, data=data)
        terminate()

    sleep(10)
