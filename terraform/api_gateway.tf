resource "aws_api_gateway_rest_api" "Whiskeyduler" {
  name = "WhiskeydulerApi"
  description = "This API schedules docker jobs in spot instance."
}

resource "aws_api_gateway_resource" "WhiskeydulerList" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  parent_id = "${aws_api_gateway_rest_api.Whiskeyduler.root_resource_id}"
  path_part = "list"
}

resource "aws_api_gateway_method" "WhiskeydulerListGet" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerList.id}"
  http_method = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "WhiskeydulerListGet200" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerList.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerListGet.http_method}"
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "WhiskeydulerListGet200" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerList.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerListGet.http_method}"
  status_code = "${aws_api_gateway_method_response.WhiskeydulerListGet200.status_code}"
}

resource "aws_api_gateway_integration" "WhiskeydulerListGet" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerList.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerListGet.http_method}"
  type = "AWS"
  integration_http_method = "POST" # Must be POST for invoking Lambda function
  credentials = "${aws_iam_role.APIGatewayInvokeLambda.arn}"
  # http://docs.aws.amazon.com/apigateway/api-reference/resource/integration/#uri
  uri = "arn:aws:apigateway:us-west-2:lambda:path/2015-03-31/functions/${aws_lambda_function.WhiskeydulerList.arn}/invocations"
}

resource "aws_api_gateway_resource" "WhiskeydulerSchedule" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  parent_id = "${aws_api_gateway_rest_api.Whiskeyduler.root_resource_id}"
  path_part = "schedule"
}

resource "aws_api_gateway_method" "WhiskeydulerSchedulePost" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerSchedule.id}"
  http_method = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "WhiskeydulerSchedulePost200" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerSchedule.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerSchedulePost.http_method}"
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "WhiskeydulerSchedulePost200" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerSchedule.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerSchedulePost.http_method}"
  status_code = "${aws_api_gateway_method_response.WhiskeydulerSchedulePost200.status_code}"
}

resource "aws_api_gateway_integration" "WhiskeydulerSchedulePost" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerSchedule.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerSchedulePost.http_method}"
  type = "AWS"
  integration_http_method = "POST" # Must be POST for invoking Lambda function
  credentials = "${aws_iam_role.APIGatewayInvokeLambda.arn}"
  # http://docs.aws.amazon.com/apigateway/api-reference/resource/integration/#uri
  uri = "arn:aws:apigateway:us-west-2:lambda:path/2015-03-31/functions/${aws_lambda_function.WhiskeydulerSchedule.arn}/invocations"
}

resource "aws_api_gateway_resource" "WhiskeydulerStatus" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  parent_id = "${aws_api_gateway_rest_api.Whiskeyduler.root_resource_id}"
  path_part = "status"
}

resource "aws_api_gateway_method" "WhiskeydulerStatusPost" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerStatus.id}"
  http_method = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "WhiskeydulerStatusPost200" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerStatus.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerStatusPost.http_method}"
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "WhiskeydulerStatusPost200" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerStatus.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerStatusPost.http_method}"
  status_code = "${aws_api_gateway_method_response.WhiskeydulerStatusPost200.status_code}"
}

resource "aws_api_gateway_integration" "WhiskeydulerStatusPost" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerStatus.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerStatusPost.http_method}"
  type = "AWS"
  integration_http_method = "POST" # Must be POST for invoking Lambda function
  credentials = "${aws_iam_role.APIGatewayInvokeLambda.arn}"
  # http://docs.aws.amazon.com/apigateway/api-reference/resource/integration/#uri
  uri = "arn:aws:apigateway:us-west-2:lambda:path/2015-03-31/functions/${aws_lambda_function.WhiskeydulerStatus.arn}/invocations"
}

resource "aws_api_gateway_resource" "WhiskeydulerCallback" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  parent_id = "${aws_api_gateway_rest_api.Whiskeyduler.root_resource_id}"
  path_part = "callback"
}

resource "aws_api_gateway_method" "WhiskeydulerCallbackPost" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerCallback.id}"
  http_method = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "WhiskeydulerCallbackPost200" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerCallback.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerCallbackPost.http_method}"
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "WhiskeydulerCallbackPost200" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerCallback.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerCallbackPost.http_method}"
  status_code = "${aws_api_gateway_method_response.WhiskeydulerCallbackPost200.status_code}"
}

resource "aws_api_gateway_integration" "WhiskeydulerCallbackPost" {
  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  resource_id = "${aws_api_gateway_resource.WhiskeydulerCallback.id}"
  http_method = "${aws_api_gateway_method.WhiskeydulerCallbackPost.http_method}"
  type = "AWS"
  integration_http_method = "POST" # Must be POST for invoking Lambda function
  credentials = "${aws_iam_role.APIGatewayInvokeLambda.arn}"
  # http://docs.aws.amazon.com/apigateway/api-reference/resource/integration/#uri
  uri = "arn:aws:apigateway:us-west-2:lambda:path/2015-03-31/functions/${aws_lambda_function.WhiskeydulerCallback.arn}/invocations"
}

resource "aws_api_gateway_deployment" "prod" {
  depends_on = ["aws_api_gateway_integration_response.WhiskeydulerSchedulePost200",
                "aws_api_gateway_integration_response.WhiskeydulerListGet200",
                "aws_api_gateway_integration_response.WhiskeydulerStatusPost200",
                "aws_api_gateway_integration_response.WhiskeydulerCallbackPost200"]

  rest_api_id = "${aws_api_gateway_rest_api.Whiskeyduler.id}"
  stage_name = "prod"

  provisioner "local-exec" {
    command = "sleep 10 && aws apigateway create-deployment --rest-api-id ${aws_api_gateway_rest_api.Whiskeyduler.id} --stage-name prod && sed -i 's/PASTE_THE_API_ID_HERE/${aws_api_gateway_rest_api.Whiskeyduler.id}/' ../scripts/check_job.py"
  }
}

output "API ENDPOINT"  {
    value = "https://${aws_api_gateway_rest_api.Whiskeyduler.id}.execute-api.us-west-2.amazonaws.com/prod/"
}
