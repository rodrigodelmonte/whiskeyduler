resource "aws_cloudwatch_event_rule" "every_hour" {
    name = "every_hour"
    description = "Fires every hour"
    schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "WhiskeydulerReSchedule_every_hour" {
    rule = "${aws_cloudwatch_event_rule.every_hour.name}"
    target_id = "whiskeyduler_reschedule_every_hour"
    arn = "${aws_lambda_function.WhiskeydulerReSchedule.arn}"
}
