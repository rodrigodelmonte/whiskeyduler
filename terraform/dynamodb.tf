resource "aws_dynamodb_table" "Whiskeyduler" {
    name = "whiskeydulerdb"
    read_capacity = 5
    write_capacity = 5
    hash_key = "job"
    attribute {
      name = "job"
      type = "S"
    }
}
