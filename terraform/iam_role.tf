resource "aws_iam_role" "APIGatewayInvokeLambda" {
  name = "APIGatewayInvokeLambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "APIGatewayInvokeLambda" {
  name = "APIGatewayInvokeLambdaPolicy"
  role = "${aws_iam_role.APIGatewayInvokeLambda.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": [
        "*"
      ],
      "Action": [
        "lambda:InvokeFunction"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role" "APIGatewayLambdaRole" {
 name = "APIGatewayLambdaRole"
 assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Effect": "Allow",
     "Principal": {
       "Service": "lambda.amazonaws.com"
     },
     "Action": "sts:AssumeRole"
   }
 ]
}
EOF
}

resource "aws_iam_role_policy" "APIGatewayLambdaPolicy" {
 name = "APIGatewayLambdaPolicy"
 role = "${aws_iam_role.APIGatewayLambdaRole.id}"
 policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": [
       "logs:*"
     ],
     "Effect": "Allow",
     "Resource": "*"
   }
 ]
}
EOF
}

resource "aws_iam_policy_attachment" "APIGatewayLambdaEC2" {
    name = "AmazonEC2FullAccess"
    roles = ["${aws_iam_role.APIGatewayLambdaRole.name}"]
    policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

resource "aws_iam_policy_attachment" "APIGatewayLambdaIAM" {
    name = "IAMFullAccess"
    roles = ["${aws_iam_role.APIGatewayLambdaRole.name}"]
    policy_arn = "arn:aws:iam::aws:policy/IAMFullAccess"
}

resource "aws_iam_policy_attachment" "APIGatewayLambdaDynamo" {
    name = "AmazonDynamoDBFullAccess"
    roles = ["${aws_iam_role.APIGatewayLambdaRole.name}"]
    policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role" "SpotInstanceDefaultRole" {
 name = "spot-instance-default-role"
 assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Effect": "Allow",
     "Principal": {
       "Service": "ec2.amazonaws.com"
     },
     "Action": "sts:AssumeRole"
   }
 ]
}
EOF
}

resource "aws_iam_instance_profile" "SpotInstanceDefaultRole" {
    name = "spot-instance-default-role"
    roles = ["${aws_iam_role.SpotInstanceDefaultRole.name}"]
}

resource "aws_iam_policy_attachment" "SpotInstanceDefaultRoleS3" {
    name = "AmazonS3FullAccess"
    roles = ["${aws_iam_role.SpotInstanceDefaultRole.name}"]
    policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_policy_attachment" "SpotInstanceDefaultRoleEC2" {
    name = "AmazonEC2FullAccess"
    roles = ["${aws_iam_role.SpotInstanceDefaultRole.name}"]
    policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}
