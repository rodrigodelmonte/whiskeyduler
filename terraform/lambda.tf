resource "aws_lambda_function" "WhiskeydulerCallback" {
    filename = "../lambda/WhiskeydulerCallback.zip"
    function_name = "WhiskeydulerCallback"
    description = "This function is a callback for jobs terminated."
    role = "${aws_iam_role.APIGatewayLambdaRole.arn}"
    handler = "lambda_function.lambda_handler"
    source_code_hash = "${base64sha256(file("../lambda/WhiskeydulerCallback.zip"))}"
    runtime = "python2.7"
    timeout = 10
}

resource "aws_lambda_function" "WhiskeydulerSchedule" {
    filename = "../lambda/WhiskeydulerSchedule.zip"
    function_name = "WhiskeydulerSchedule"
    description = "This function schedule docker jobs."
    role = "${aws_iam_role.APIGatewayLambdaRole.arn}"
    handler = "lambda_function.lambda_handler"
    source_code_hash = "${base64sha256(file("../lambda/WhiskeydulerSchedule.zip"))}"
    runtime = "python2.7"
    timeout = 3
}

resource "aws_lambda_function" "WhiskeydulerReSchedule" {
    filename = "../lambda/WhiskeydulerReSchedule.zip"
    function_name = "WhiskeydulerReSchedule"
    description = "This function Reschedule docker jobs terminated by spot instance market."
    role = "${aws_iam_role.APIGatewayLambdaRole.arn}"
    handler = "lambda_function.lambda_handler"
    source_code_hash = "${base64sha256(file("../lambda/WhiskeydulerReSchedule.zip"))}"
    runtime = "python2.7"
    timeout = 3
}

resource "aws_lambda_function" "WhiskeydulerList" {
    filename = "../lambda/WhiskeydulerList.zip"
    function_name = "WhiskeydulerList"
    description = "This function list scheduled docker jobs."
    role = "${aws_iam_role.APIGatewayLambdaRole.arn}"
    handler = "lambda_function.lambda_handler"
    source_code_hash = "${base64sha256(file("../lambda/WhiskeydulerList.zip"))}"
    runtime = "python2.7"
    timeout = 3
}

resource "aws_lambda_function" "WhiskeydulerStatus" {
    filename = "../lambda/WhiskeydulerStatus.zip"
    function_name = "WhiskeydulerStatus"
    description = "This function show status from scheduled docker jobs."
    role = "${aws_iam_role.APIGatewayLambdaRole.arn}"
    handler = "lambda_function.lambda_handler"
    source_code_hash = "${base64sha256(file("../lambda/WhiskeydulerStatus.zip"))}"
    runtime = "python2.7"
    timeout = 3
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_WhiskeydulerReschedule" {
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.WhiskeydulerReSchedule.function_name}"
    principal = "events.amazonaws.com"
    source_arn = "${aws_cloudwatch_event_rule.every_hour.arn}"
}
