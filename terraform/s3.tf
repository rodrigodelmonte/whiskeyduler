resource "aws_s3_bucket" "WhiskeyduleBucketScripts" {

    bucket = "whiskeyduler-script"
    acl = "private"

}

resource "aws_s3_bucket" "WhiskeyduleBucketOutput" {

    bucket = "whiskeyduler-output-job"
    acl = "private"

}

resource "aws_s3_bucket_object" "WhiskeyduleBucketScripts" {
    bucket = "${aws_s3_bucket.WhiskeyduleBucketScripts.id}"
    key = "check_job.py"
    source = "../scripts/check_job.py"
    etag = "${md5(file("../scripts/check_job.py"))}"

    depends_on = ["aws_api_gateway_deployment.prod"]
}
